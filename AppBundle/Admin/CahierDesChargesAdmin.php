<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class CahierDesChargesAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
	        $formMapper
	        ->add('title', 'text')
	        ->add('body', 'textarea')
	    ;

	}

	protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('draft')
        ;
    }

    public function toString($object)
    {
        return $object instanceof CahierDesCharges
            ? $object->getTitle()
            : 'Cahier Des Charges'; // shown in the breadcrumb on the create view
    }
}